using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class BiomaInfo : ScriptableObject
{
    [Serializable]
    public struct ItemSpawn
    {
        public Item item;
        public float spawnProbability;
    }
    [Serializable]
    public struct BiomaTileInfo
    {
        public string name;
        public Tile tile;
        public float probability; 
        public float itemProbability; 
        public ItemSpawn[] spawns;
    }
    public BiomaTileInfo[] tiles;
}
