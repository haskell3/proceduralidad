using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapGenerator : MonoBehaviour
{
    [SerializeField]
    private bool m_Verbose = false;

    [Serializable]
    public struct Bioma
    {
        public BiomaInfo bioma;
        public float distance;
    }
    public Bioma[] biomes;

    [SerializeField]
    private Tilemap m_Tilemap;
    [SerializeField]
    private Tilemap m_TilemapItem;

    [Header("Size")]
    //size of the area we will paint
    [SerializeField]
    private int m_Width;
    [SerializeField]
    private int m_Height;

    [Header("Base Parameters")]
    [SerializeField]
    //offset from the perlin map
    private float m_OffsetX;
    [SerializeField]
    private float m_OffsetY;
    [SerializeField]
    private float m_Frequency = 4f;
    [SerializeField]
    private float m_FrequencyItem = 4f;

    //octaves
    private const int MAX_OCTAVES = 8;
    [Header("Octave Parameters")]
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_Octaves = 0;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_OctavesItem = 0;
    [Range(2, 3)]
    [SerializeField]
    private int m_Lacunarity = 2;
    [Range(2, 3)]
    [SerializeField]
    private int m_LacunarityItem = 2;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_Persistence = 0.5f;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_PersistenceItem = 0.5f;
    [SerializeField]
    [Min(1)]
    private int m_TilemapToPerlinUnits;
    [SerializeField]
    private int m_Soroll;

    private float[] dangerRadios;

    // Start is called before the first frame update
    void Start()
    {
        dangerRadios = new float[biomes.Length];
        for (int i = 0; i < biomes.Length; i++)
        {
            dangerRadios[i] = (m_Height / 2) * biomes[i].distance;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            GenerateTerrain();
        }
    }

    private void GenerateTerrain()
    {
        m_Tilemap.ClearAllTiles();
        m_TilemapItem.ClearAllTiles();

        dangerRadios = new float[biomes.Length];
        for (int i = 0; i < biomes.Length; i++)
        {
            dangerRadios[i] = (m_Height / 2) * biomes[i].distance;
        }

        for (int y = -m_Height / 2; y < m_Height / 2; y++)
        {
            for (int x = -m_Width / 2; x < m_Width / 2; x++)
            {
                Bioma currentBioma = biomes[biomes.Length - 1];
                float distancewFromFarm = new Vector2(x, y).magnitude + UnityEngine.Random.Range(-m_Soroll, m_Soroll);
                for (int i = 0; i < biomes.Length; i++)
                {
                    if (distancewFromFarm < dangerRadios[i])
                    {
                        currentBioma = biomes[i];
                        break;
                    }
                }
                float perlin = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_Frequency, m_Width, m_Height, m_OffsetX, m_OffsetY, m_Octaves, m_Lacunarity, m_Persistence);
                Tile tile = currentBioma.bioma.tiles[0].tile;
                BiomaInfo.ItemSpawn[] itemSpawns = currentBioma.bioma.tiles[0].spawns;
                float spawnProbability = 0;
                for (int i = 0; i < currentBioma.bioma.tiles.Length; i++)
                {
                    if (perlin < currentBioma.bioma.tiles[i].probability)
                    {
                        spawnProbability = currentBioma.bioma.tiles[0].itemProbability;
                        itemSpawns = currentBioma.bioma.tiles[i].spawns;
                        tile = currentBioma.bioma.tiles[i].tile;
                        break;
                    }
                }

                /*if ( < dangerRadioValue)
                {
                    if (perlin < m_PerlinSector[0])
                    {
                        //roca
                        tile = m_Tiles[0];
                    }
                    else 
                    {
                        //bosque
                        tile = m_Tiles[1];
                    }
                }
                else if (new Vector2(x, y).magnitude + UnityEngine.Random.Range(-m_Soroll, m_Soroll) < dangerRadioValueAmazing)
                {
                    if (perlin < m_PerlinSector[0])
                    {
                        //roca
                        tile = m_Tiles[0];
                    }
                    else if(perlin < m_PerlinSector[1])
                    {
                        //bosque frondos
                        tile = m_Tiles[1];
                    }
                    else if (perlin < m_PerlinSector[2])
                    {
                        //bosque
                        tile = m_Tiles[2];
                    }
                    else
                    {
                        //Swampert
                        tile = m_Tiles[3];
                    }
                } else
                {
                    if (perlin < m_PerlinSector[0])
                    {
                        //roca
                        tile = m_Tiles[0];
                    }
                    else if (perlin < m_PerlinSector[2])
                    {
                        //bosque frondos
                        tile = m_Tiles[2];
                    }
                    else
                    {
                        //Swampert
                        tile = m_Tiles[3];
                    }
                }*/



                for (int i = 0; i < m_TilemapToPerlinUnits; i++)
                    for (int j = 0; j < m_TilemapToPerlinUnits; j++)
                    {
                        Vector3Int pos = new Vector3Int(m_TilemapToPerlinUnits * x + i, m_TilemapToPerlinUnits * y + j, 0);
                        m_Tilemap.SetTile(pos, tile);
                        perlin = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_FrequencyItem, m_Width, m_Height, m_OffsetX + 100, m_OffsetY + 100, m_OctavesItem, m_LacunarityItem, m_PersistenceItem);
                        if (perlin <= spawnProbability)
                        {
                            for (int k = 0; k < itemSpawns.Length; k++)
                            {
                                if (UnityEngine.Random.Range(0, 101) < itemSpawns[k].spawnProbability)
                                {
                                    m_TilemapItem.SetTile(pos, itemSpawns[k].item.info.tile);
                                    break;
                                }
                            }
                        }
                    }
            }
        }

    }
}
