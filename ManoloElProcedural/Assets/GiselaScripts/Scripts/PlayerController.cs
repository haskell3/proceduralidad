using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rigid;
    private Rigidbody2D rigidcamera;
    [SerializeField] private float speed;
    [SerializeField] private float camera_speed;
    [SerializeField] private GameObject m_Camera;
    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        rigidcamera = m_Camera.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 move = Vector2.zero;
        Vector2 movecamera = Vector2.zero;
        if (Input.GetKey(KeyCode.W)) 
        {
            move += Vector2.up;
        }else if (Input.GetKey(KeyCode.A))
        {
            move -= Vector2.right;
        }else if (Input.GetKey(KeyCode.S))
        {
            move -= Vector2.up;
        }else if (Input.GetKey(KeyCode.D))
        {
            move += Vector2.right;
        }

        rigid.velocity = move.normalized * speed;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            movecamera += Vector2.up;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            movecamera -= Vector2.right;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            movecamera -= Vector2.up;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            movecamera += Vector2.right;
        }
        rigidcamera.velocity = movecamera.normalized * camera_speed;
    }
}
