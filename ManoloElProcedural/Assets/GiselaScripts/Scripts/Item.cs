using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

[CreateAssetMenu]
public class Item : ScriptableObject
{
    [Serializable]
    public struct ItemInfo
    {
        public string name;
        public int tier;
        public Tile tile;
    }
    public ItemInfo info;
}
